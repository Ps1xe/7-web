import { useState } from 'react';

/**
 * Компонент карточки. Должен принимать пропсы id, name
 * Мы должны отображать name как имя карточки.
 */
export function Card({ name, onDelete }) {
  const [isDeleteLoading, setIsDeleteLoading] = useState(false);

  /**
   * Метод удаления текущей карточки
   */
  const handleDelete = async () => {
    setIsDeleteLoading(true);

    /**
     * Вызовем onDelete
     */
    onDelete();

    setIsDeleteLoading(false);
  };

  return (
    <div>
      <b>{name}</b>
      <button
        //! Не трогайте строку ниже -- она нужна для тестов !
        id="delete_card_button"
        //По клику вызовем handleDelete
        onClick={()=>handleDelete()}
        disabled={isDeleteLoading}
      >
        Delete card
      </button>
    </div>
  );
}
