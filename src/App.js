import React, { useEffect, useState } from 'react';
import { api } from './api';
import { Column, Header } from './components';

function App() {
  //Объявим state для хранения юзера
  // useState();
  const [userName, setUserName] = useState("");
  //Объявим state для хранения колонок
  // useState();
  const [columnName, setColumnName] = useState([])
  const [columns, setColumns] = useState("");
  const [isAddColumnLoading, setIsAddColumnLoading] = useState(false);

  /**
   * Функция, которая делает запрос к API и создает новую колонку
   * (!) Обратите внимание, что мы храним
   *  в isAddColumnLoading статус выполнения запроса: загружается он или нет
   */
  const handleCreateColumn = async () => {
    setIsAddColumnLoading(true);

    /**
     * Здесь вызываем метод API на создание колонки
     * (!) После успешного создания колонки через API
     *  мы должны добавить ее в локальное состояние хранящее колонки,
     *  чтобы отобразить ее
     */
    const newColumn= await api.createColumn(columnName);

    setColumns([newColumn,...columns]);

    /**
     * (?) Возможно, здесь еще стоит сбрасывать состояние инпута для имени
     *  новой колонки.
     */
    setColumnName("");
    setIsAddColumnLoading(false);
  };

  /**
   * Метод для удаления карточки.
   * Он должен делать запрос к API
   *  и затем удалять карточку из локального состояния
   * @param {number} columnId
   */
  const handleDeleteColumn = async (columnId) => {
    /**
     * 1. API call
     * 2. Change columns state
     */
     if(await api.deleteColumn(columnId)){
        setColumns(columns.filter((column) => column.id !== columnId));
    }};

  /**
   * Метод для создания карточки в колонке с ID columnId.
   * Делает запрос к API и добавляет карточку в локальное состояние
   * @param {number} columnId
   * @param {number} cardName
   */
  const handleCreateCard = async (columnId, cardName) => {
    /**
     * 1. API call
     * 2. Change cards state
     */
     const card = await api.createCard(columnId, cardName);
     const column = columns.find((column) => column.id === columnId);
     column.cards = [...column.cards? column.cards : [], card]
     setColumns([column,...columns.filter((column)=> column.id !== columnId)])
   };

  /**
   * Метод для удаления карточки с ID cardId
   * Делает запрос к API и удаляет карточку и локального состояния
   * @param {number} cardId
   */
  const handleDeleteCard = async (cardId) => {
    /**
     * 1. API call
     * 2. Change cards state
     */
     await api.deleteCard(cardId);
     const column = columns.find((column) => column.cards.find((card) => card.id === cardId));
     column.cards = column.cards.filter((card)=>card.id !== cardId)
     setColumns([column, ...columns.filter((col)=> col.id !== column.id)])
  };

  /**
   * Функция подгрузки колонок и юзера -- начального состояния приложения.
   * Она должна запрашивать юзера и колонки с API, класть их в локальное состояние
   * */
  const fetchState = async () => {
    /**
     * 1. API call
     * 2. Change columns state
     * 3. Change user state
     */
    // const userName = await api.getCurrentUser();
    // setUserName(userName.name);
    // const columns = await api.getColumns();
    // setColumns(columns);
    const user= await api.getCurrentUser();
    setUserName(user.name);
    const columns = await api.getColumns();
    setColumns(columns);

  };

  /**
   * Task: На useEffect-е мы должны вызывать функцию fetchState.
   * Эта функция должна запрашивать с API: текущего юзера и все колонки
   */

  // useEffect()
  useEffect(()=>{fetchState()},[]);


  const renderColumns = () => {
    //Если колонки грузятся, то покажем загрузку
    if (!columns) {
      return <div>Loading...</div>;
    }

    //Если их нету -- пустой экран
    if (columns.length === 0) {
      return (
        <div>
          Nothing to show :( <br /> Create first column!
        </div>
      );
    }

    //Иначе -- отобразим все колонки
    return columns.map((column) => {
      return (
        <Column
          //Пропишем все пропсы, которые хочет Column (см. компонент Column)
          id ={column.id}
          key={column.id}
          name={column.name}
          cards={column.cards ? column.cards:[]}
          onDelete={()=>handleDeleteColumn(column.id)}
          onAddCard={handleCreateCard}
          onDeleteCard={handleDeleteCard}
        />
      );
    });
  };

  return (
    <div>
      {/* В Header мы должны передать имя текущего пользователя */}
       {<Header name={userName}/>}
      <div 
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          marginBottom: '24px',
        }}
      >
        {renderColumns()}
      </div>
      <div>
        <form>
          <input
            //! Не трогайте строку ниже -- она нужна для тестов !
            id="create_column_input" 
            placeholder="Column name"
            // Нужно хранить value-инпута в локальном состоянии
            value={columnName}
            // И менять его на новое введенное значение на событии onChange
            // (!) новое введенное значение в подписчике можно получить так:
            // (event) => event.target.value (<-- tadaaa 🎉)
            onChange={(event)=>{setColumnName(event.target.value)}}
          />
          <button
            //! Не трогайте строку ниже -- она нужна для тестов !
            id="create_column_button"
            type="button"
            // По клику -- создаем колонку!
            onClick={()=>handleCreateColumn()}
            // Пока создание колонки "загружается" переводим колонку в disabled
            disabled={isAddColumnLoading}
          >
            Create
          </button>
        </form>
      </div>
    </div>
  );
}

export default App;
